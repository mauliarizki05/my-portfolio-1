function populate(){
    if(quiz.isEnded()){
        // showScores();
    }
    else {
        var element = document.getElementById("question");
        element.innerHTML = quiz.getQuestionIndex().text;
    
        //show choices
        var choices = quiz.getQuestionIndex().choices;
        for(var i = 0; i< choices.length; i++){
            var element = document.getElementById("choice" + i);
            element.innerHTML = choices[i];
            guess("btn" + i, choices[i]);
        }
        showProgress();
    }
};

function showScores(){
     var gameOverHtml = "<h1>Result</h1>";
         gameOverHtml += "<h2 id='score'>Your scores: " + quiz.score + "</h2>"  
         var element = document.getElementById("quiz");
         element.innerHTML = gameOverHtml;
}

function guess(id, guess){
    var button = document.getElementById(id);
    button.onclick = function(){
        quiz.guess(guess);
        populate();
    }
};


function showProgress(){
    var currentQuestionNumber = quiz.questionIndex + 1;
    var element = document.getElementById("progress");
    element.innerHTML = "Question" + currentQuestionNumber + "of" + quiz.questions.length;
}

var questions =[
    new Question("which one is not an object oriented programming languange?",["Java", "c#", "c++", "c"], "c"),
    new Question("which languange is used for styling web pages?",["Html", "Jquery", "CSS", "c"], "CSS"), 
    new Question("where are ____ main components of object oriented programming",["1", "6", "2", "4"], "4"),
    new Question("Which languange is used for web apps",["Php", "Phyton", "Javascript", "All"], "All"),
    new Question("MVC is a  _____.", ["languange", "Library", "Framework", "all"], "Framework"),
];

var quiz = new Quiz(questions);

populate();